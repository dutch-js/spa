<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    const HOURS = 1;
    const MINUTE = 2;
    const SECONDS = 3;
    protected $table = 'services';
    protected $fillable = ['id','name', 'price', 'image', 'description', 'bonus', 'time', 'unit_time', 'created_at', 'updated_at'];
    public $timestamps = true;
}
