<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    const PAID = 1;
    const UNPAID = 2;

    protected $table = 'orders';
    protected $fillable = ['id','name', 'address', 'email', 'phone', 'description', 'total', 'type', 'quantity'];
    public $timestamps = true;

}
