<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderService extends Model
{
    const ORDER = 1;
    const SUCCESS = 2;
    const DENY = 3;
    const END = 4;
    protected $table = 'orders_services';
    protected $fillable = ['id','name', 'phone', 'email', 'time', 'service_id', 'type'];
    public $timestamps = true;
}
