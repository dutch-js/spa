<?php

namespace App\Http\Controllers\Admin;

use App\Customer;
use App\Http\Requests\AddMemberRequest;
use App\Http\Requests\EditMemberRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MemberController extends Controller
{
    public function index()
    {
        $users = Customer::select('id','name', 'email', 'updated_at')
            ->orderBy('updated_at', 'DESC')
            ->paginate(DEFAULT_PAGINATION_PER_PAGE);
        return view('admin.member.index', ['users'=>$users]);
    }

    public function form_add()
    {
        return view('admin.member.add');
    }
    public function add(AddMemberRequest $request)
    {
        $data = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()

        ];
        Customer::insert($data);

        return redirect(route('member.index'))
            ->with('alert-success', trans('messages.successfully_created', ['name' => 'Người dùng']));
    }

    public function form_edit($id)
    {
        $user = Customer::select('id')->where('id',$id)->firstOrFail();
        return view('admin.member.edit', ['user' => $user]);
    }

    public function edit($id, EditMemberRequest $request)
    {
        Customer::where('id', trim($id))
            ->update(
                [
                    'password' => bcrypt($request->input('password')),
                ]
            );

        return redirect(route('member.index'))
            ->with('alert-success', trans('messages.successfully_updated', ['name' => 'Người dùng']));
    }
    public function delete($id)
    {
        Customer::findOrFail(trim($id));
        Customer::where('id', trim($id))->delete();

        return redirect(route('member.index'))
            ->with('alert-success', trans('messages.successfully_deleted', ['name' => 'Người dùng']));
    }
}
