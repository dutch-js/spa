<?php

namespace App\Http\Controllers\Admin;

use App\Customer;
use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\HistoryRequest;
use App\Service;
use App\User;
use Carbon\Carbon;

use Illuminate\Http\Request;

class HistoryController extends Controller
{
    public function index($id=0)
    {
        $histories = History::select('history.id', 'customer.name', 'services.name AS service', 'history.updated_at')
            ->join('customer', 'customer.id', '=', 'history.user_id')
            ->join('services', 'services.id', '=', 'history.service_id')
            ->orderBy('history.id', 'DESC');
        if ($id !=0) {
            $histories = $histories->where('history.user_id', '=', $id);
        }
        $histories = $histories->paginate(DEFAULT_PAGINATION_PER_PAGE);
        return view('admin.history.index', ['histories' =>$histories]);
    }

    public function form_add()
    {
        $users = Customer::select('id', 'name')
            ->get();
        $services = Service::select('id', 'name')->get();
        return view('admin.history.add', ['users'=> $users, 'services'=>$services]);
    }

    public function add(HistoryRequest $request)
    {
        History::insert(
            [
                'user_id' => $request->input('user'),
                'service_id' => $request->input('service'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        );

        return redirect(route('history.index'))
            ->with('alert-success', trans('messages.successfully_created', ['name' => 'Lịch sử']));

    }
}
