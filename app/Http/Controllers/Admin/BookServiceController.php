<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\OrderService;
use Carbon\Carbon;

class BookServiceController extends Controller
{
    public function index()
    {
        $orders = OrderService::select('id', 'name', 'email', 'type', 'phone', 'time')->orderBy('id', 'DESC')
            ->paginate(DEFAULT_PAGINATION_PER_PAGE);
        return view('admin.order-service.index', ['orders' => $orders]);
    }
    public function edit($id, $type)
    {
        $data = [
            'type' => $type,
            'updated_at' => Carbon::now()
        ];
        OrderService::where('id', trim($id))->update($data);

        return redirect(route('book-service.index'))
            ->with('alert-success', 'Trạng thái được cập nhật thành công');
    }
}
