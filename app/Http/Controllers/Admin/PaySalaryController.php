<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PaySalaryRequest;
use App\Paysalary;
use App\Salary;
use App\Service;
use App\TimeKeeping;
use App\User;
use Carbon\Carbon;
class PaySalaryController extends Controller
{
    public function index()
    {
        $pays = Paysalary::select('pay_salary.id', 'pay_salary.month', 'pay_salary.year', 'pay_salary.total', 'pay_salary.type', 'users.name')
            ->join('users', 'pay_salary.user_id', '=', 'users.id')
            ->orderBy('pay_salary.id', 'DESC')
            ->paginate(DEFAULT_PAGINATION_PER_PAGE);
        return view('admin.pay-salary.index', ['pays' => $pays]);
    }
    public function form_add()
    {
        $users = User::select('id', 'name')->whereIn('type', [User::TV, User::KTV, User::LT])
            ->get();
        return view('admin.pay-salary.add', ['users'=>$users]);
    }
    public function add(PaySalaryRequest $request)
    {
        $salary = Salary::select('salary')->where('user_id', $request->input('user'))->first();
        if(empty($salary)) {
            return redirect(route('pay-salary.index'))->with('alert-danger', 'Bạn phải thêm lương người dùng');
        }
        $total = 0;
        $times = TimeKeeping::select('id', 'user_id', 'service_id')
            ->whereMonth('time', $request->input('month'))
            ->whereYear('time', $request->input('year'))
            ->where('user_id', $request->input('user'))
            ->get();
        $arr = [];
        if (count($times) > 0) {
            foreach ($times as $time) {
                if(!empty($time->service_id)) {
                    foreach (json_decode($time->service_id) as $js) {
                        $arr[] = $js;
                    }
                }
            }

            $diff = array_unique($arr);
            $data = [];
            foreach ($diff as $val) {
                $counts = array_count_values($arr);
                $data[] = ['id' => $val, 'cou' => $counts[$val]];
            }
            $idService = [];
            foreach ($data as $dat) {
                $idService[] = $dat['id'];
            }
            $services = Service::select('id', 'bonus')
                ->whereIn('id', $idService)
                ->get();
            if (count($services) > 0) {
                foreach ($data as $da) {
                    foreach ($services as $service) {
                        if ($da['id'] == $service->id) {
                            $total+= $da['cou']*$service->bonus;
                        }
                    }
                }
            }
        }

        Paysalary::create(
            [
                'month' => $request->input('month'),
                'year' => $request->input('year'),
                'user_id' => $request->input('user'),
                'type' => Paysalary::UNPAID,
                'total' => $salary->salary+$total,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        );
        return redirect(route('pay-salary.index'))
            ->with('alert-success', trans('messages.successfully_created', ['name' => 'Lương']));

    }
    public function update($id)
    {
        Paysalary::where('id', trim($id))->update(['type' => Paysalary::PAID]);
        return redirect(route('pay-salary.index'))
            ->with('alert-success', 'Cập nhật trạng thái thành công');
    }

}
