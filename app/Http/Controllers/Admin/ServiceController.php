<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\EditServiceRequest;
use App\Http\Requests\ServiceRequest;
use App\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class ServiceController extends Controller
{
    public function index()
    {
        $services = Service::select('id', 'name', 'image', 'price', 'description', 'bonus', 'time', 'unit_time')
            ->orderBy('updated_at', 'DESC')
            ->paginate(DEFAULT_PAGINATION_PER_PAGE);
        return view('admin.service.index', ['services' => $services]);
    }

    public function form_add()
    {
        return view('admin.service.add');
    }

    public function add(ServiceRequest $request)
    {
        $fileName = time().'assadl.'.time().pathinfo($request->file('image-service')->getClientOriginalName(), PATHINFO_EXTENSION);
        $request->file('image-service')->storeAs('public/service', $fileName);

        Service::create(
            [
                'image' => $fileName,
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'price' => $request->input('price'),
                'bonus' => $request->input('bonus'),
                'time' => $request->input('time'),
                'unit_time' => $request->input('unit_time'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        );

        return redirect(route('service.index'))
            ->with('alert-success', trans('messages.successfully_created', ['name' => 'Dịch vụ']));
    }
    public function form_edit($id)
    {

        $service = Service::select('id', 'name', 'image', 'price', 'description', 'bonus', 'time', 'unit_time')
            ->where('id', trim($id))
            ->firstOrFail();

        return view('admin.service.edit', ['service' => $service]);
    }


    public function edit($id, EditServiceRequest $request)
    {
        $data = [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'bonus' => $request->input('bonus'),
            'time' => $request->input('time'),
            'unit_time' => $request->input('unit_time'),
            'price' => $request->input('price'),
            'updated_at' => Carbon::now()
        ];
        if ($request->hasFile('image-service')) {
            $fileName = time().'assadl.'.time().'.'.pathinfo($request->file('image-service')->getClientOriginalName(), PATHINFO_EXTENSION);
            $request->file('image-service')->storeAs('public/service', $fileName);
            $data['image'] = $fileName;

        }
        Service::where('id', trim($id))->update($data);

        return redirect(route('service.index'))
            ->with('alert-success', trans('messages.successfully_updated', ['name' => 'Dịch vụ']));
    }
    public function delete($id)
    {
        Service::findOrFail(trim($id));
        Service::where('id', trim($id))->delete();

        return redirect(route('service.index'))
            ->with('alert-success', trans('messages.successfully_deleted', ['name' => 'Dịch vụ']));
    }

    public function detail($id)
    {
        $service = Service::select('id', 'name', 'image', 'price', 'description', 'bonus', 'time', 'unit_time')->where('id', $id)->firstOrFail();
        return view('admin.service.detail', ['service' => $service]);
    }

    public function image($id)
    {
        $photo = Product::where('id', trim($id))
            ->first();

        if (empty($photo)) {
            return abort(404);
        }
        $path = storage_path().'/app/public/product/'.$photo->image;
        echo file_get_contents($path);
        return true;

    }
}
