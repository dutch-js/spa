<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookServiceRequest;
use App\OrderService;
use App\Service;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ServiceOrderController extends Controller
{
    public function index($id)
    {
        $service = Service::select('id', 'price', 'name')
            ->where('id', trim($id))
            ->firstOrFail();
        return view('frontend.book_service',['service' =>$service]);
    }

    public function add($id, BookServiceRequest $request)
    {
        $order = OrderService::create([
            'name' =>$request->input('name'),
            'email' => $request->input('email'),
            'service_id' => $id,
            'type' => OrderService::ORDER,
            'time' => date('Y-m-d', strtotime($request->time)),
            'phone' => $request->input('phone'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        return redirect()->back()->with('alert-success', 'Đặt dịch vụ thành công');
    }
}
