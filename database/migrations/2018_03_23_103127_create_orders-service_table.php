<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
        {
            Schema::create('orders_services', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name', 1000);
                $table->string('phone', 20);
                $table->string('email', 100);
                $table->timestamp('time');
                $table->integer('service_id')->unsigned();
                $table->foreign('service_id')->references('id')->on('services')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_services');
    }
}
