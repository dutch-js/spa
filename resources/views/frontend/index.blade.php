    @extends('frontend.layouts.app')
@section('sidebar')
    @parent
@endsection
@section('content')
    <!-- About us -->
    <div class="about-3">
        <h3 class="heading-agileinfo">Welcome<span>Skin,Nails and Beauty Services</span></h3>
        <div class="container">
            <div class="d-flex">

                <div class="about1">
                    <h5>Nullam pulvinar vulputate aliquam. Pellentesque venenatis ut mi ac porta. Praesent interdum nibh libero, id malesuada libero aliquet quis. Donec at odio nibh.</h5>
                    <p>If you like nature, picturesque views, beautiful horses and quality time spending, our riding school is the perfect
                        choice to begin your acquaintance with equitation, or to simply enjoy monthly family events at our facility.</p>
                    <p>kuytase uaeraquis autem vel eum iure reprehend unicmquam eius, Basmodi temurer sehsmunim.</p>
                    <a class="join-wthree" href="about.html">About Us
                        <i class="fa fa-hand-o-right" aria-hidden="true"></i>
                    </a>
                </div>
                <div class="about2">

                </div>
            </div>

        </div>
    </div>
    <!-- //About us -->
    <!-- wthree-mid -->
    <div class="wthree-mid">
        <div class="container">
            <h3>Nisl amet dolor sit ipsum veroeros sed blandit</h3>
            <p>Standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
            <div class="botton">
                <a class="join-wthree" href="#" data-toggle="modal" data-target="#myModal">Read More
                    <i class="fa fa-hand-o-right" aria-hidden="true"></i>
                </a>
            </div>
        </div>
    </div>
    <!-- pricing -->
    @if(count($services) > 0)
    <div class="w3ls-section wthree-pricing">
        <div class="container">

            <h3 class="heading-agileinfo">Các dịch vụ<span>Da, Móng tay và Dịch vụ Làm đẹp</span></h3>
                @foreach($services as $service)
                    <div class="pricing-grid grid-two">
                        <div class="w3ls-top">
                            <h3>{{$service->name}}</h3>
                        </div>
                        <div class="w3ls-bottom">
                            <h4> {{number_format($service->price,0 ,',', '.')}}<span class="sup">vnđ</span> </h4>
                            <ul class="count">
                                <li>
                                    <img src="{{asset('storage/service/'.$service->image)}}" alt="{{$service->name}}" class="img-responsive"style="max-height: 300px">
                                </li>
                            </ul>

                            <div class="more">
                                <a href="{{route('book.service', $service->id)}}">Đặt ngay</a>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="clearfix"> </div>
                <!--End-slider-script-->
            </div>
        </div>
    </div>
    @endif
    <!--//pricing-->

    <!-- Clients -->
    <div class="clients">
        <div class="container">
            <div class="wthree_head_section">
                <h3 class="heading-agileinfo">Testimonials<span>Skin,Nails and Beauty Services</span></h3>
            </div>


            <section class="slider">
                <div class="flexslider">
                    <ul class="slides">
                        <li>
                            <img src="images/t1.jpg" alt="" />
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation .</p>
                            <div class="client">
                                <h5>Gerald Roy</h5>
                            </div>
                        </li>
                        <li>
                            <img src="images/t2.jpg" alt="" />
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation .</p>
                            <div class="client">
                                <h5>Adam Brandom</h5>
                            </div>
                        </li>
                        <li>
                            <img src="images/t3.jpg" alt="" />
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation .</p>
                            <div class="client">
                                <h5>Steve Artur</h5>
                            </div>
                        </li>
                        <li>
                            <img src="images/t4.jpg" alt="" />
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation .</p>
                            <div class="client">
                                <h5>Martin Victor</h5>
                            </div>
                        </li>
                    </ul>
                </div>
            </section>
        </div>
    </div>
    <!-- //Clients -->
@endsection