<!DOCTYPE html>
<html lang="en">
<head>
    <title>REDOUND SPA</title>
    <!-- custom-theme -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Pedicure Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); } </script>

    <!-- for banner css files -->
    <link rel="stylesheet" type="text/css" href="{{asset('front-end/web/css/zoomslider.css')}}" /><!--zoomslider css -->
    <script type="text/javascript" src="{{asset('front-end/web/js/modernizr-2.6.2.min.js')}}"></script><!--modernizer css -->
    <!-- //for banner css files -->
    <link rel="stylesheet" href="{{asset('front-end/web/css/flexslider.css')}}" type="text/css" media="all" />
    <!-- custom-theme css files -->
    <link href="{{asset('front-end/web/css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{asset('front-end/web/css/style.css')}}" rel="stylesheet" type="text/css" media="all" />
    <!-- //custom-theme css files-->
    <!-- font-awesome-icons -->
    <link href="{{asset('front-end/web/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{ asset('jquery-ui/jquery-ui.css') }}" rel="stylesheet">
    <!-- //font-awesome-icons -->
    <!-- googlefonts -->
    <link href="//fonts.googleapis.com/css?family=PT+Serif:400,700" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
    <!-- //googlefonts -->
</head>
<body>
<!--/main-header-->
@section('sidebar')
<div id="demo-1" data-zs-src='["{{asset('front-end/web/images/b3.jpg')}}", "{{asset('front-end/web/images/b2.jpg')}}", "{{asset('front-end/web/images/b1.jpg')}}"]' data-zs-overlay="dots">
    <div class="demo-inner-content">

        <!--/banner-info-->
        <div class="header">
            <div class="w3-agile-logo">
                <div class="container">
                    <div class=" head-wl">
                        <div class="headder-w3">
                            <h1><a href="{{route('frontend.index')}}">REDOUND</a></h1>
                        </div>

                        <div class="w3-header-top-right-text">
                            <h6 class="caption"> Liên hệ</h6>
                            <p>(111)+245 245 66</p>
                        </div>

                        <div class="email-right">
                            <h6 class="caption">Địa chỉ email</h6>
                            <p><a href="mailto:mail@example.com" class="info"> info@example.com</a></p>

                        </div>


                        <div class="agileinfo-social-grids">
                            <h6 class="caption"></h6>
                            <ul>
                                @guest
                                    <li><a href="{{ route('member.login') }}">Login</a></li>
                                    <li><a href="{{ route('member.register') }}">Register</a></li>
                                @else
                                    <li>
                                        <a>{{ Auth::user()->name }}</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                @endguest
                            </ul>
                        </div>

                        <div class="clearfix"> </div>
                    </div>
                </div>

            </div>
            <div class="top-nav">
                <nav class="navbar navbar-default navbar-fixed-top">

                    <!-- //header -->
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <div id="navbar" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav ">
                                <li class="{{ Request::is('/') ? 'active' : '' }}"><a href="{{route('frontend.index')}}">Trang chủ</a></li>
                                <li class="{{ Request::is('about') ? 'active' : '' }}"><a href="{{route('frontend_about')}}">Thông tin</a></li>
                                <li class="{{ Request::is('service') ? 'active' : '' }}"><a href="{{route('service.list')}}">Dịch vụ</a></li>
                                <li class="{{ Request::is('product') ? 'active' : '' }}"><a href="{{route('frontend.product')}}">Sản phẩm</a></li>
                                <li class="{{ Request::is('contact') ? 'active' : '' }}"><a href="{{route('frontend_contact')}}">Liên hệ</a></li>
                                <li><a href="{{route('all_cart.product')}}"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i><span style="color: white;">{{\Cart::count()}} </span></a></li>
                            </ul>
                        </div>

                    </div>
                    <div class="clearfix"> </div>
                </nav>
            </div>
        </div>
        <div class="w3-banner-head-info">
            <div class="container">
                <div class="banner-text">
                    <h2>Take Care of Your Nails
                        <span>Paint Them Color Awesome!</span></h2>

                </div>
            </div>
        </div>
        <!--/banner-info-->
    </div>
</div>
<!--/banner-section-->
@show
<div class="flash-message text-center">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(session('alert-' . $msg))
            <div class="alert alert-{{ $msg }}">
                {{ session('alert-' . $msg) }}
            </div>
        @endif
    @endforeach
</div>
@yield('content')
<!-- Subscribe -->
<div class="wthree-subscribef-w3ls">
    <div class="container">
        <h3 class="tittlef-agileits-w3layouts white-clrf">Get All Special Offers!</h3>
        <p class="white-clrf">Sign up for our newsletter and get all the latest tips and tricks to polish your nails at a good rate!</p>
        <div class="footer_w3layouts_gridf_right">

            <form action="#" method="post">
                <input type="email" name="Email" placeholder="Email address..." required="">
                <input type="submit" value="Submit">
                <div class="clearfix"></div>
            </form>
        </div>
    </div>
</div>
<!--// Subscribe -->
<!-- footer -->
<div class="footer_top_agileits">
    <div class="container">
        <div class="col-md-4 footer_grid">
            <h3>About Us</h3>
            <p>Nam libero tempore cum vulputate id est id, pretium semper enim. Morbi viverra congue nisi vel pulvinar posuere sapien
                eros.
            </p>
        </div>
        <div class="col-md-4 footer_grid">
            <h3>Latest News</h3>
            <ul class="footer_grid_list">
                <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                    <a href="#" data-toggle="modal" data-target="#myModal">Lorem ipsum neque vulputate </a>
                </li>
                <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                    <a href="#" data-toggle="modal" data-target="#myModal">Dolor amet sed quam vitae</a>
                </li>
                <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                    <a href="#" data-toggle="modal" data-target="#myModal">Lorem ipsum neque, vulputate </a>
                </li>
                <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                    <a href="#" data-toggle="modal" data-target="#myModal">Dolor amet sed quam vitae</a>
                </li>
                <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                    <a href="#" data-toggle="modal" data-target="#myModal">Lorem ipsum neque, vulputate </a>
                </li>
            </ul>
        </div>
        <div class="col-md-4 footer_grid">
            <h3>Contact Info</h3>
            <ul class="address">
                <li><i class="fa fa-map-marker" aria-hidden="true"></i>8088 USA, Honey block, <span>New York City.</span></li>
                <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:info@example.com">info@example.com</a></li>
                <li><i class="fa fa-phone" aria-hidden="true"></i>+09187 8088 9436</li>
            </ul>
        </div>
        <div class="clearfix"> </div>

    </div>
</div>
<div class="footer_w3ls">
    <div class="container">
        <div class="footer_bottom1">
            <p>© 2018 Pedicure. All rights reserved | Design by <a href="http://w3layouts.com">W3layouts</a></p>
        </div>
    </div>
</div>
<!-- //footer -->
<!-- bootstrap-modal-pop-up -->
<div class="modal video-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                Pedicure
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <img src="images/g4.jpg" alt=" " class="img-responsive" />
                <p>Ut enim ad minima veniam, quis nostrum
                    exercitationem ullam corporis suscipit laboriosam,
                    nisi ut aliquid ex ea commodi consequatur? Quis autem
                    vel eum iure reprehenderit qui in ea voluptate velit
                    esse quam nihil molestiae consequatur, vel illum qui
                    dolorem eum fugiat quo voluptas nulla pariatur.
                    <i>" Quis autem vel eum iure reprehenderit qui in ea voluptate velit
                        esse quam nihil molestiae consequatur.</i></p>
            </div>
        </div>
    </div>
</div>
<!-- //bootstrap-modal-pop-up -->
<!-- js -->
<script type="text/javascript" src="{{asset('front-end/web/js/jquery-2.1.4.min.js')}}"></script>
<!-- for bootstrap working -->
<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<!-- //js -->
<!-- for banner js file-->
<script type="text/javascript" src="{{asset('front-end/web/js/jquery.zoomslider.min.js')}}"></script><!-- zoomslider js -->
<!-- //for banner js file-->
<!-- for smooth scrolling -->
<script src="{{asset('front-end/web/js/SmoothScroll.min.js')}}"></script>
<script type="text/javascript" src="{{asset('front-end/web/js/move-top.js')}}"></script>
<script type="text/javascript" src="{{asset('front-end/web/js/easing.js')}}"></script>
<!-- here stars scrolling icon -->
<script type="text/javascript">
    $(document).ready(function() {
        /*
            var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear'
            };
        */

        $().UItoTop({ easingType: 'easeOutQuart' });

    });
</script>
<!-- //here ends scrolling icon -->
<!-- //for smooth scrolling -->
<!-- scrolling script -->
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<!-- //scrolling script -->
<!-- FlexSlider-JavaScript -->
<script defer src="{{asset('web/js/jquery.flexslider.js')}}"></script>
<script src="{{ asset('jquery-ui/jquery-ui.js') }}"></script>
<script type="text/javascript">

    $(window).load(function(){
        $('.flexslider').flexslider({
            animation: "slide",
            start: function(slider){
                $('body').removeClass('loading');
            }
        });
    });
</script>
<script>
    $(document).ready(function () {
        $( function() {
            $( "#datepicker" ).datepicker();
        } );
    })
</script>
<!-- //FlexSlider-JavaScript -->

</body>
</html>