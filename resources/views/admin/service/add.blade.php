@extends('admin.layouts.app')
@section('title', 'Add product')
@section('content')
    <div class="inner-block">
        <div class="product-block">
            <div class="pro-head">
                <h2>Thêm dịch vụ mới</h2>
            </div>
            <div class="error">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="col-md-12 product-grid">
                <form action="{{route('service.add')}}" method="POST" role="form" class="form-add-info" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="">Tên dịch vụ</label>
                        <input type="text" class="form-control <?php echo $errors->has('name') ? 'input-error' : '';?>" name="name" value="{{old('name')}}" placeholder="Nhập tên dịch vụ">
                    </div>
                    <div class="form-group">
                        <label for="">Giá dịch vụ</label>
                        <input type="number" class="form-control <?php echo $errors->has('price') ? 'input-error' : '';?>" name="price" value="{{old('price')}}" placeholder="Nhập giá dịch vụ">
                    </div>
                    <div class="form-group">
                        <label for="">Phụ cấp</label>
                        <input type="number" class="form-control <?php echo $errors->has('bonus') ? 'input-error' : '';?>" name="bonus" value="{{old('bonus')}}" placeholder="Nhập lương thưởng cho nhân viên">
                    </div>
                    <div class="form-group">
                        <label for="">Thời gian</label>
                        <input type="number" class="form-control <?php echo $errors->has('time') ? 'input-error' : '';?>" name="time" value="{{old('time')}}" placeholder="Thời gian hoàn thành">
                    </div>
                    <div class="form-group">
                        <label for="">Đơn vị thời gian</label>
                        <select name="unit_time">
                            <option value="{{\App\Service::HOURS}}">Giờ</option>
                            <option value="{{\App\Service::MINUTE}}">Phút</option>
                            <option value="{{\App\Service::SECONDS}}">Giây</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <div class="show_image">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="">{{trans('messages.image')}} </label>
                        <input type="file" class="form-control <?php echo $errors->has('image-service') ? 'input-error' : '';?>" name="image-service">
                    </div>
                    <div class="form-group">
                        <label>Mô tả</label>
                        <textarea class="trumbowyg" name="description">{{old('description')}}</textarea>
                    </div>
                    <button type="submit" class="btn btn-primary submit-form">Thêm</button>
                </form>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
@endsection