@extends('admin.layouts.app')
@section('title', 'history')
@section('content')
    <div class="inner-block">
        <h2>Lịch sử người dùng
            <a href="{{route('history.add.form')}}" class="pull-right btn btn-primary">Thêm lịch sử</a>
        </h2>
        <table class="table table-condensed">
            <thead>
            <tr>
                <th>{{trans('messages.time')}}</th>
                <th>Tên người dùng</th>
                <th>Tên dịch vụ</th>
            </tr>
            </thead>
            <tbody>
            @forelse($histories as $service)
                <tr>
                    <td>{{$service->updated_at}}</td>
                    <td>{{$service->name}}</td>
                    <td>{{$service->service}}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="12"><h2 class="text-center no_data">{{ trans('messages.no_data') }}</h2></td>
                </tr>
            @endforelse
            </tbody>
        </table>
        <!-- pagination -->
        <div class="row text-center">
            {{ $histories->links() }}
        </div>
    </div>
@endsection