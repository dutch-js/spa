<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});
Route::group(['namespace' => 'Auth'], function () {
    Route::get('logout', 'LogoutController@logout');
});

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth', 'role:admin']], function () {
    Route::get('/', 'AdminController@index')->name('admin.index');
    // Category
    Route::group(['prefix' => 'category'], function () {
        Route::get('index', 'CategoryController@index')->name('category.index');
        Route::get('add', 'CategoryController@form_add')->name('category.add.form');
        Route::post('add', 'CategoryController@add')->name('category.add');
        Route::get('edit/{id}', 'CategoryController@form_edit')->name('category.edit.form');
        Route::put('edit/{id}', 'CategoryController@edit')->name('category.edit');
        Route::delete('delete{id}', 'CategoryController@delete')->name('category.delete');
    });

    // Brands
    Route::group(['prefix' => 'brand'], function () {
        Route::get('index', 'BrandController@index')->name('brand.index');
        Route::get('add', 'BrandController@form_add')->name('brand.add.form');
        Route::post('add', 'BrandController@add')->name('brand.add');
        Route::get('edit/{id}', 'BrandController@form_edit')->name('brand.edit.form');
        Route::put('edit/{id}', 'BrandController@edit')->name('brand.edit');
        Route::delete('delete/{id}', 'BrandController@delete')->name('brand.delete');
//            Route::get('image/{id}', 'BrandController@image')->name('brand.image');
    });

    // product
    Route::group(['prefix' => 'product'], function () {
        Route::get('index', 'ProductController@index')->name('product.index');
        Route::get('add', 'ProductController@form_add')->name('product.add.form');
        Route::post('add', 'ProductController@add')->name('product.add');
        Route::get('edit/{id}', 'ProductController@form_edit')->name('product.edit.form');
        Route::put('edit/{id}', 'ProductController@edit')->name('product.edit');
        Route::delete('delete/{id}', 'ProductController@delete')->name('product.delete');
        Route::get('detail/{id}', 'ProductController@detail')->name('product.detail');
    });

    // service
    Route::group(['prefix' => 'service'], function () {
        Route::get('index', 'ServiceController@index')->name('service.index');
        Route::get('add', 'ServiceController@form_add')->name('service.add.form');
        Route::post('add', 'ServiceController@add')->name('service.add');
        Route::get('edit/{id}', 'ServiceController@form_edit')->name('service.edit.form');
        Route::put('edit/{id}', 'ServiceController@edit')->name('service.edit');
        Route::delete('delete/{id}', 'ServiceController@delete')->name('service.delete');
        Route::get('detail/{id}', 'ServiceController@detail')->name('service.detail');
    });

    // history
    Route::group(['prefix' => 'history'], function () {
        Route::get('index/{id?}', 'HistoryController@index')->name('history.index');
        Route::get('add', 'HistoryController@form_add')->name('history.add.form');
        Route::post('add', 'HistoryController@add')->name('history.add');
    });

    // user
    Route::group(['prefix' => 'user'], function () {
        Route::get('index', 'UserController@index')->name('user.index');
        Route::get('add', 'UserController@form_add')->name('user.add.form');
        Route::post('add', 'UserController@add')->name('user.add');
        Route::get('edit/{id}', 'UserController@form_edit')->name('user.edit.form');
        Route::put('edit/{id}', 'UserController@edit')->name('user.edit');
        Route::delete('delete/{id}', 'UserController@delete')->name('user.delete');
    });
    // member
    Route::group(['prefix' => 'member'], function () {
        Route::get('index', 'MemberController@index')->name('member.index');
        Route::get('add', 'MemberController@form_add')->name('member.add.form');
        Route::post('add', 'MemberController@add')->name('member.add');
        Route::get('edit/{id}', 'MemberController@form_edit')->name('member.edit.form');
        Route::put('edit/{id}', 'MemberController@edit')->name('member.edit');
        Route::delete('delete/{id}', 'MemberController@delete')->name('member.delete');
    });

    // salary
    Route::group(['prefix' => 'salary'], function () {
        Route::get('index', 'SalaryController@index')->name('salary.index');
        Route::get('add', 'SalaryController@form_add')->name('salary.add.form');
        Route::post('add', 'SalaryController@add')->name('salary.add');
        Route::get('edit/{id}', 'SalaryController@form_edit')->name('salary.edit.form');
        Route::put('edit/{id}', 'SalaryController@edit')->name('salary.edit');
        Route::delete('delete/{id}', 'SalaryController@delete')->name('salary.delete');
    });
    // time keeping
    Route::group(['prefix' => 'time-keeping'], function () {
        Route::get('index', 'TimeKeepingController@index')->name('time-keeping.index');
        Route::get('add', 'TimeKeepingController@form_add')->name('time-keeping.add.form');
        Route::post('add', 'TimeKeepingController@add')->name('time-keeping.add');
        Route::get('detail/{user_id}', 'TimeKeepingController@detail')->name('time-keeping.detail');
    });

    // order
    Route::group(['prefix' => 'order'], function () {
        Route::get('index', 'OrderController@index')->name('order.index');
        Route::get('edit/{id}', 'OrderController@form_edit')->name('order.edit.form');
        Route::put('edit/{id}', 'OrderController@edit')->name('order.edit');
    });

    // book service
    Route::group(['prefix' => 'book-service'], function () {
        Route::get('index', 'BookServiceController@index')->name('book-service.index');
        Route::get('edit/{id}/{type}', 'BookServiceController@edit')->name('book-service.edit');
    });

    // pay salary
    Route::group(['prefix' => 'pay-salary'], function () {
        Route::get('index', 'PaySalaryController@index')->name('pay-salary.index');
        Route::get('change-type/{id}', 'PaySalaryController@update')->name('pay-salary.update');
        Route::get('add', 'PaySalaryController@form_add')->name('pay-salary.form_add');
        Route::post('add', 'PaySalaryController@add')->name('pay-salary.add');
    });

    Route::get('/dang-nhap',     ['as' => 'member.login' ,'uses' => 'Member\MemberController@showLoginForm']);
    Route::post('/dang-nhap',     ['as' => 'member.login.submit' ,'uses' => 'Member\MemberController@login']);
    Route::get('/dang-ky',       ['as'  => 'member.register' ,      'uses' =>'Member\MemberController@getRegister']);
    Route::post('/dang-ky',       ['as'  => 'member.register.submit' ,      'uses' =>'Member\MemberController@postRegister']);

    // statis
    Route::group(['prefix' => 'statis'], function () {
        Route::get('index', 'StatiscalController@index')->name('statis.index');
    });

//    });


});

Route::get('/', 'IndexController@index')->name('frontend.index');

Route::get('cart/{id}', 'CartController@cart')->name('cart.product');
Route::get('all-cart', 'CartController@all_cart')->name('all_cart.product');
Route::get('delete-cart/{rowId}', 'CartController@delete')->name('cart_delete');
Route::put('update-cart/{rowId}', 'CartController@update')->name('cart_update');
Route::post('customer-order', 'CartController@order')->name('cart_order');


Route::get('book-service/{id}', 'ServiceOrderController@index')->name('book.service');
Route::post('book-service/{id}', 'ServiceOrderController@add')->name('book.service.add');



Route::get('service', 'ServiceController@index')->name('service.list');
Route::get('service-detail/{id}', 'ServiceController@detail')->name('frontend.service.detail');
Route::get('product/{id?}', 'ProductController@index')->name('frontend.product');
Route::get('product/detail/{id}', 'ProductController@detail')->name('frontend.product.detail');
Route::get('about', 'IndexController@about')->name('frontend_about');
Route::get('contact', 'IndexController@contact')->name('frontend_contact');


Route::get('dang-nhap', 'MemberController@showLoginForm')->name('member.login');
Route::post('dang-nhap', 'MemberController@login')->name('member.login.submit');
Route::get('dang-ky', 'MemberRegisterController@getRegister')->name('member.register');
Route::post('dang-ky', 'MemberRegisterController@postRegister')->name('member.register.submit');


Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
